﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DeckViewer
{
    public static class CardsInfo
    {
        public class CardItem
        {
            public string name { get; set; }
            public string image { get; set; }
            public string type { get; set; }
            public int mana { get; set; }
        }

        public static Dictionary<string, CardItem> cardIdToCard = new Dictionary<string, CardItem>();
        public static Dictionary<string, CardItem> nameToCard = new Dictionary<string, CardItem>();
        public static List<string> cardNameList = new List<string>();

        public static CardItem GetCardInfo(string cardId)
        {
            if (cardIdToCard.ContainsKey(cardId))
            {
                return cardIdToCard[cardId];
            }

            return null;
        }

        public static int GetCardMana(string cardName)
        {
            if (nameToCard.ContainsKey(cardName))
            {
                return nameToCard[cardName].mana;
            }

            return 0;
        }

        public static void Load()
        {
            using (StreamReader r = new StreamReader("./cards.json"))
            {
                var items = JsonConvert.DeserializeObject(r.ReadToEnd()) as JContainer;

                foreach (var card in items["cards"].ToObject<List<CardItem>>())
                {
                    var cardId = card.image.Split('/').Last().Split('.')[0];
                    cardIdToCard[cardId] = card;
                    nameToCard[card.name] = card;

                    if (card.type == "minion" || card.type == "spell" || card.type == "weapon")
                    {
                        cardNameList.Add(card.name);
                    }
                }
            }
        }
    }
}
